﻿using UnityEngine;
using System.Collections;

using UnityEngine.UI;

public class GameController : MonoBehaviour
{

//	public float speed;
//	public Text countText;
//	public Text winText;
//	private Rigidbody2D rb2d;
//	private int count;
//	private Vector2 oldPosition;
//	private Vector2 newPosition;
//	private bool inMove;

	private const int UP = 1;
	private const int DOWN = 2;
	private const int LEFT = 3;
	private const int RIGHT = 4;

	public Button executar;
	public GameObject blocksPanel;
//
	private PlayerController playerController;
	private bool pcSet;
	int atual;


//	public int[] commandList;

	void Start ()
	{
//		rb2d = GetComponent<Rigidbody2D> ();
//		count = 0;
//		winText.text = "";
//		SetCountText ();
//		oldPosition.Set (0.0f, 0.0f);
//		inMove = false;
		pcSet = false;
		atual =0;
	}
	//Fixed
	void Update ()
	{
		//		float moveHorizontal = Input.GetAxis ("Horizontal");
		//		float moveVertical = Input.GetAxis ("Vertical");
		//		Vector2 movement = new Vector2 (moveHorizontal, moveVertical);

		//		rb2d.AddForce (movement * speed);

		//		switch (com->command) {
		//		case UP:
		//			//comando do up
		//			break;
		//
		//		case DOWN:
		//			//comando do down
		//			break;
		//
		//		case LEFT:
		//			//comando do left
		//			break;
		//
		//		case RIGHT:
		//			//comando do left
		//			break;
		//		}
		if(pcSet){
			if (atual < playerController.getQuantCom ()) {
				if (!playerController.IsMoving () && !this.IsInvoking ("goToNext")) {
					playerController.blockMovement ();
					switch (playerController.getMove (atual)) {
					case UP:
						playerController.MoveUp ();
						this.Invoke ("goToNext", 2);
						break;
				
					case DOWN:
						playerController.MoveDown ();
						this.Invoke ("goToNext", 2);
						break;
				
					case LEFT:
						playerController.MoveLeft ();
						this.Invoke ("goToNext", 2);
						break;
				
					case RIGHT:
						playerController.MoveRight ();
						this.Invoke ("goToNext", 2);
						break;
					}
				}
			} else {
				Debug.Log ("Terminaram os comandos");
				executar.interactable = true;
				blocksPanel.SetActive (true);
				pcSet = false;
				atual = 0;
				playerController.endGame ();
			}
		}
	}

	public void SetPlayerControllerReference(PlayerController player){

		Debug.Log ("Setou player controller");
		playerController = player;
		pcSet = true;
		executar.interactable = false;
		blocksPanel.SetActive (false);
		//runGame ();
		//rodar player Controller
	}
	void goToNext(){
		atual++;
		playerController.freeMovement ();
	}
//	private void runGame(){
//		playerController.movePlayer
//	}
}
