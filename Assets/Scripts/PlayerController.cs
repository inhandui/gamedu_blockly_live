﻿using UnityEngine;
using System.Collections;

using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{

	public float speed;
	public Text countText;
	public Text winText;
	private Rigidbody2D rb2d;
	private int count;
	private Vector2 oldPosition;
	private Vector2 newPosition;
	private bool inMove;

	private const int UP = 1;
	private const int DOWN = 2;
	private const int LEFT = 3;
	private const int RIGHT = 4;

	public GameController gameController;

	public GameObject UpButton;
	public GameObject DownButton;
	public GameObject LeftButton;
	public GameObject RightButton;

	public GameObject canvas;
	/* posição inicial dos comandos na tela */
	private int commandsX;
	private int commandsY;
	private int commandsZ;

	public int[] commandList; //49 = quantidade máxima de "casas" do mapa do jogo
//	public GameObject[] UICommandList;
	private int quantCom; //quantidade de comandos

	void Start ()
	{
		rb2d = GetComponent<Rigidbody2D> ();
		count = 0;
		winText.text = "";
		SetCountText ();
		oldPosition.Set (0.0f, 0.0f);
		inMove = false;
		quantCom = 0;
		commandList = new int[49];
//		UICommandList = new GameObject[49];
		commandsX = 200;
		commandsY = 750;
		commandsZ = 0;
	}
	//Fixed
	void Update ()
	{
//		float moveHorizontal = Input.GetAxis ("Horizontal");
//		float moveVertical = Input.GetAxis ("Vertical");
//		Vector2 movement = new Vector2 (moveHorizontal, moveVertical);

//		rb2d.AddForce (movement * speed);

//		switch (com->command) {
//		case UP:
//			//comando do up
//			break;
//
//		case DOWN:
//			//comando do down
//			break;
//
//		case LEFT:
//			//comando do left
//			break;
//
//		case RIGHT:
//			//comando do left
//			break;
//		}

		//quebrar o if para fazer alguma animação de quando colidir com a parede 
		if (!inMove && Input.GetKeyDown ("up") && (oldPosition.y < (3.0f * 3.6f))) {
			inMove = true;
			print ("up arrow key is held down");
			movePlayer (new Vector2 (0.0f, 3.6f));
		}
			
		//quebrar o if para fazer alguma animação de quando colidir com a parede 
		if (!inMove && Input.GetKeyDown ("down") && (oldPosition.y > (3.0f * (-3.6f)))) {
			inMove = true;
			print ("down arrow key is held down");
			movePlayer (new Vector2 (0.0f, -3.6f));
		}
			
		//quebrar o if para fazer alguma animação de quando colidir com a parede 
		if (!inMove && Input.GetKeyDown ("left") && (oldPosition.x > (3.0f * (-3.6f)))) {
			inMove = true;
			print ("left arrow key is held down");
			movePlayer (new Vector2 (-3.6f, 0.0f));
		}
			
		//quebrar o if para fazer alguma animação de quando colidir com a parede 
		if (!inMove && Input.GetKeyDown ("right") && (oldPosition.x < (3.0f * (3.6f)))) {
			inMove = true;
			print ("right arrow key is held down");
			movePlayer (new Vector2 (3.6f, 0.0f));
		}
	}


	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.gameObject.CompareTag ("PickUp")) {
			other.gameObject.SetActive (false);
			count++;
			SetCountText ();
		}
		//fazer alguma animação de quando colidir com a parede 
		if (other.gameObject.CompareTag ("BackGround")) {
			Debug.Log ("Colidiu com a parede do backgroud");
			rb2d.MovePosition (oldPosition);
		}

//		if (other.gameObject.CompareTag ("UP")) {
//			Debug.Log ("Pressionou o botão cima");
//			//adicionar botao a lista de execucao dos comandos
//		}
//
//		if (other.gameObject.CompareTag ("Down")) {
//			Debug.Log ("Pressionou o botão baixo");
////			com->command = DOWN;
//			//adicionar botao a lista de execucao dos comandos
//		}
//
//		if (other.gameObject.CompareTag ("Left")) {
//			Debug.Log ("Pressionou o botão esquerda");
////			com->command = LEFT;
//			//adicionar botao a lista de execucao dos comandos
//		}
//
//		if (other.gameObject.CompareTag ("Right")) {
//			Debug.Log ("Pressionou o botão direita");
////			com->command = RIGHT;
//			//adicionar botao a lista de execucao dos comandos
//		}

	}

	void SetCountText ()
	{
		countText.text = "Itens Coletados: " + count.ToString ();
		if (count >= 9) {
			winText.text = "Você venceu!";
		}
	}

	public void movePlayer (Vector2 position)
	{
		newPosition = oldPosition + position;
		rb2d.MovePosition (newPosition);
		oldPosition = newPosition;
		inMove = false;
	}

	//função para saber se o player está se movendo
	public bool IsMoving ()
	{
		return inMove;
	}

	//fução para bloquear a variavel que indica movimento e assim impedir outro movimento de ser executado enquanto o anterior não terminar
	public bool blockMovement ()
	{
		if (inMove) {
			return false;
		}

		inMove = true;
		return true;
	}

	//fução para liberar a variavel que indica movimento e assim impedir outro movimento de ser executado enquanto o anterior não terminar
	public void freeMovement ()
	{
		inMove = false;
	}

	public void addMove(int move){
		//Debug.Log("quant = " + quantCom + " move = " + move);
//		UICommandList[quantCom] = createMoveUI (move);
		createMoveUI (move);
		commandList [quantCom] = move;	
		quantCom++;
	}

	private void createMoveUI(int move){

		GameObject button;

		switch (move) {
		case UP:
			button = (GameObject)Instantiate (UpButton, calcPosition(), Quaternion.identity);
			button.transform.SetParent (canvas.transform);
			button.SetActive (true);
			break;
		case DOWN:
			button = (GameObject)Instantiate (DownButton, calcPosition(), Quaternion.identity);
			button.transform.SetParent (canvas.transform);
			button.SetActive (true);
			break;
		case LEFT:
			button = (GameObject)Instantiate (LeftButton, calcPosition(), Quaternion.identity);
			button.transform.SetParent (canvas.transform);
			button.SetActive (true);
			break;
		case RIGHT:
			button = (GameObject)Instantiate (RightButton, calcPosition(), Quaternion.identity);
			button.transform.SetParent (canvas.transform);
			button.SetActive (true);
			break;
		}

	}

	public Vector3 calcPosition(){
		if( quantCom == 4 || quantCom == 8 || quantCom == 12 || quantCom == 16 || quantCom == 20 || quantCom == 24) {
			commandsX = 300;
			commandsY = commandsY - 100;
			return (new Vector3(commandsX, commandsY, commandsZ));
		}
		else{
			commandsX = commandsX + 100;
			//commandsY = commandsY - 50;
		}

		return (new Vector3(commandsX, commandsY, commandsZ));
	}

//	private GameObject createMoveUI(int move){
//		GameObject retObj = null;
//		switch (move) {
//		case UP:
//			retObj = (GameObject) Instantiate (UpButton, new Vector3 (0, 0, 0), Quaternion.identity);
//			break;
//		case DOWN:
//			retObj = (GameObject)Instantiate (DownButton, new Vector3 (0, 0, 0), Quaternion.identity);
//			break;
//		case LEFT:
//			retObj = (GameObject)Instantiate (LeftButton, new Vector3 (0, 0, 0), Quaternion.identity);
//			break;
//		case RIGHT:
//			retObj = (GameObject)Instantiate (RightButton, new Vector3 (0, 0, 0), Quaternion.identity);
//			break;
//		}
//
//		return retObj;
//	}

	public void setPlayerControllerReferenceOnGameController(){
		gameController.GetComponentInParent<GameController>().SetPlayerControllerReference (this);
	}

	public int getQuantCom(){
		return quantCom;
	}

	public void setQuantCom(int valor){
		quantCom = valor;
	}

	public Vector2 getOldPosition(){
		return oldPosition;
	}

	public Vector2 getNewPosition(){
		return newPosition;
	}
//
//	public void setOldPosition(int valor){
//		oldPosition = oldPosition + valor;
//	}

	public void MoveUp(){
		if(oldPosition.y < (3.0f * 3.6f)) {
			print ("up arrow key is held down");
			movePlayer (new Vector2 (0.0f, 3.6f));
		}
	}

	public void MoveDown(){
		if(oldPosition.y > (3.0f * (-3.6f))) {
			print ("down arrow key is held down");
			movePlayer (new Vector2 (0.0f, -3.6f));
		}
		
	}

	public void MoveLeft(){
		if(oldPosition.x > (3.0f * (-3.6f))) {
			print ("left arrow key is held down");
			movePlayer (new Vector2 (-3.6f, 0.0f));
		}
	}

	public void MoveRight(){
		if(oldPosition.x < (3.0f * (3.6f))) {
			print ("right arrow key is held down");
			movePlayer (new Vector2 (3.6f, 0.0f));
		}
	}

	public int getMove(int move){
		return commandList [move];
	}

	public void endGame(){
		inMove = false;
		quantCom = 0;
		commandList = new int[49];
	}

}
